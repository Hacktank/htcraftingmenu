include("Scripts/UI/View.lua")

if HTCraftingMenuUIView == nil then
	HTCraftingMenuUIView = View.Subclass("HTCraftingMenuUIView")
end

local COLORS = {
	RECIPE = {
		IDLE = {
			IMAGE = {
				NORMAL = "FFFFFFFF",
				HOVER = "FF888888",
				PUSH = "FF333333",
			},
			TEXT = {
				NORMAL = "FFFFFFFF",
				HOVER = "FFFFFFFF",
				PUSH = "FFFFFFFF",
			},
		},
		SELECTED = {
			IMAGE = {
				NORMAL = "FF66FF66",
				HOVER = "FF449944",
				PUSH = "FF225522",
			},
			TEXT = {
				NORMAL = "FFFFFFFF",
				HOVER = "FFFFFFFF",
				PUSH = "FFFFFFFF",
			},
		},
	},
	HEADER = {
		IMAGE = {
			NORMAL = "tl:FFFF9664 tr:FFFF9664 bl:FFFF9664 br:FFFF9664",
		},
		TEXT = {
			NORMAL = "FF66FFFF",
		},
	},
}

local function HTSetButtonStateProperties(button,props)
	button:setProperty("NormalImageColour",props.IMAGE.NORMAL)
	button:setProperty("HoverImageColour",props.IMAGE.HOVER)
	button:setProperty("PushedImageColour",props.IMAGE.PUSH)

	button:setProperty("NormalTextColour",props.TEXT.NORMAL)
	button:setProperty("HoverTextColour",props.TEXT.HOVER)
	button:setProperty("PushedTextColour",props.TEXT.PUSH)
end

local function HTGetIconNameFromObject(object,default)
	if object then
		local dummyGameObjectInstance = object:NKGetInstance()
		if dummyGameObjectInstance then
			local iconFile = default or "TUGIcons/NoIcon"
			if dummyGameObjectInstance:GetIcon() then
				iconFile = dummyGameObjectInstance:GetIcon()
			end

			return iconFile
		end
	end

	return default
end

local function HTGetIconNameFromObjectName(objectName,default)
	local result = default

	local dummyGameObject = Eternus.GameObjectSystem:NKCreateGameObject(objectName,true)
	if dummyGameObject then
		result = HTGetIconNameFromObject(dummyGameObject)
		dummyGameObject:NKDeleteMe()
	end

	return result
end

local function HTAddNewDynamicWindow(args)
	local newItem = nil
	if args.windowName ~= nil and #args.windowName > 0 then
		newItem = EternusEngine.UI.Windows:createWindow(args.windowClassName,args.windowName)
	else
		newItem = EternusEngine.UI.Windows:createWindow(args.windowClassName)
	end

	args.targetContainer:addChild(newItem)

	for functionName, functionArgs in pairs(args.functionsToCall) do
		newItem[functionName](newItem,functionArgs)
	end

	for propertyName, propertyValue in pairs(args.properties) do
		newItem:setProperty(propertyName,propertyValue)
	end

	return newItem
end

local function HTAddNewItemIcon(args)
	local newItemIcon = HTAddNewDynamicWindow({
		targetContainer = args.targetContainer,

		windowClassName = "TUGLook/StaticImage",

		functionsToCall = {
			setArea = CEGUI.URect(
				CEGUI.UDim(args.icon_margin_scale,0),
				CEGUI.UDim(args.icon_margin_scale,0),
				CEGUI.UDim(args.icon_margin_scale,args.icon_size),
				CEGUI.UDim(args.icon_margin_scale,args.icon_size))
		},

		properties = {
			FrameEnabled = "false",
			BackgroundEnabled = "false",
			Image = args.iconName
		}
	})

	return newItemIcon
end

local function HTAddNewCategoryHeader(args)
	-- Root container
	------------------------
	local newItem = HTAddNewDynamicWindow({
		targetContainer = args.targetContainer,

		windowClassName = "DefaultWindow",

		functionsToCall = {
			setArea = CEGUI.URect(
				CEGUI.UDim(args.entry_margin_scale,0),
				CEGUI.UDim(args.entry_margin_scale,0),
				CEGUI.UDim(1 - args.entry_margin_scale,0),
				CEGUI.UDim(args.entry_margin_scale,args.entry_height)),
			setMouseInputPropagationEnabled = true
		},

		properties = {
		}
	})

	-- Image
	------------------------
	local newItemImage = HTAddNewDynamicWindow({
		targetContainer = newItem,

		windowClassName = "TUGLook/StaticImage",

		functionsToCall = {
			setArea = CEGUI.URect(
			CEGUI.UDim(0,0),
			CEGUI.UDim(0,args.topPadding),
			CEGUI.UDim(1,0),
			CEGUI.UDim(1,0)),
		setMouseInputPropagationEnabled = true
		},

		properties = {
			FrameEnabled = "false",
			Image = "TUGLook/Gradient",
			ImageColours = COLORS.HEADER.IMAGE.NORMAL
		}
	})

	-- Text
	------------------------
	local newItemLabel = HTAddNewDynamicWindow({
		targetContainer = newItemImage,

		windowClassName = "TUGLook/Label",

		functionsToCall = {
			setArea = CEGUI.URect(
				CEGUI.UDim(0,0),
				CEGUI.UDim(0,0),
				CEGUI.UDim(1,0),
				CEGUI.UDim(1,0)),
			setText = args.text
		},

		properties = {
			NormalTextColour = COLORS.HEADER.TEXT.NORMAL,
		}
	})

	return newItem
end

local function HTAddNewImageButton(args)
	-- Root container
	------------------------
	local newItem = HTAddNewDynamicWindow({
		targetContainer = args.targetContainer,

		windowClassName = "TUGLook/ImageButton",

		functionsToCall = {
			setArea = CEGUI.URect(
				CEGUI.UDim(args.entry_margin_scale,0),
				CEGUI.UDim(args.entry_margin_scale,0),
				CEGUI.UDim(1 - args.entry_margin_scale,0),
				CEGUI.UDim(args.entry_margin_scale,args.entry_height)),
			setMouseInputPropagationEnabled = true
		},

		properties = {
		}
	})

	-- Icon
	if args.icon then
		HTAddNewItemIcon({
			targetContainer = newItem,
			iconName = args.icon.name,
			icon_margin_scale = args.icon.margin_scale,
			icon_size = args.entry_height * (1.0 - (args.icon.margin_scale * 2))
		})
	end

	-- States
	HTSetButtonStateProperties(newItem,COLORS.RECIPE.IDLE)

	-- Alternate textures every entry
	if args.targetContainer:getChildCount() % 2 ~= 0 then
		newItem:setProperty("NormalImage","TUGLook/GradientSoft")
		newItem:setProperty("HoverImage","TUGLook/GradientSoft")
		newItem:setProperty("PushedImage","TUGLook/GradientSoft")
	else
		newItem:setProperty("NormalImage","TUGLook/Gradient")
		newItem:setProperty("HoverImage","TUGLook/Gradient")
		newItem:setProperty("PushedImage","TUGLook/Gradient")
	end

	-- Text
	newItem:setText(args.text)

	-- Event subscriptions
	for eventName, eventCallback in pairs(args.events) do
		newItem:subscribeEvent(eventName,eventCallback)
	end

	return newItem
end

local function HTAddNewItemEntry(args)
	local dummyGameObject = Eternus.GameObjectSystem:NKCreateGameObject(args.itemName,true)
	if dummyGameObject then
		-- Root container
		------------------------
		local newItem = HTAddNewDynamicWindow({
			targetContainer = args.targetContainer,

			windowClassName = "TUGLook/StaticImage",

			functionsToCall = {
				setArea = CEGUI.URect(
					CEGUI.UDim(args.entry_margin_scale,0),
					CEGUI.UDim(args.entry_margin_scale,0),
					CEGUI.UDim(1 - args.entry_margin_scale,0),
					CEGUI.UDim(args.entry_margin_scale,args.entry_height)),
				setMouseInputPropagationEnabled = true
			},

			properties = {
				FrameEnabled = "false",
				BackgroundEnabled = "false"
			}
		})

		-- Icon
		if args.icon then
			HTAddNewItemIcon({
				targetContainer = newItem,
				iconName = HTGetIconNameFromObject(dummyGameObject),
				icon_margin_scale = args.icon.margin_scale,
				icon_size = args.entry_height * (1.0 - (args.icon.margin_scale * 2))
			})
		end

		-- Alternate textures every entry
		if args.targetContainer:getChildCount() % 2 ~= 0 then
			newItem:setProperty("Image","TUGLook/GradientSoft")
		else
			newItem:setProperty("Image","TUGLook/Gradient")
		end

		-- Text
		------------------------
		local newItemLabel = HTAddNewDynamicWindow({
			targetContainer = newItem,

			windowName = "Label",

			windowClassName = "TUGLook/Label",

			functionsToCall = {
				setArea = CEGUI.URect(
					CEGUI.UDim(0,0),
					CEGUI.UDim(0,0),
					CEGUI.UDim(1,0),
					CEGUI.UDim(1,0)),
				setText = args.text
			},

			properties = {
				NormalTextColour = COLORS.RECIPE.IDLE.TEXT.NORMAL,
			}
		})
		local newItemLabelText = dummyGameObject:NKGetDisplayName()
		newItemLabel:setText(newItemLabelText)

		dummyGameObject:NKDeleteMe()

		return newItem
	end

	return nil
end

function HTCraftingMenuUIView:Constructor(layout,model)
	self.m_model = model

	self.m_rootWindow:setMouseInputPropagationEnabled(true)
	self.m_rootWindow:setMousePassThroughEnabled(true)
	self.m_rootWindow:setProperty("DistributeCapturedInputs","true")

	self.cont_validRecipes_dynamicWindows = {}
	self.sp_validRecipes = CEGUI.toScrollablePane(self.m_rootWindow:getChild("ValidRecipes/ValidRecipesScrollPane"))
	self.sp_validRecipes:setShowHorzScrollbar(false)
	self.sp_validRecipes:setMouseInputPropagationEnabled(true)

	self.cont_selectedRecipe_dynamicWindows = {}
	self.sp_selectedRecipe = CEGUI.toScrollablePane(self.m_rootWindow:getChild("SelectedRecipe/SelectedRecipeScrollPane"))
	self.sp_selectedRecipe:setShowHorzScrollbar(false)
	self.sp_selectedRecipe:setMouseInputPropagationEnabled(true)

	-- For experimental new layout
	--self.m_craftButton = self.sp_selectedRecipe:getChild("CraftButton")
	--self.m_validRecipesSearchBox = self.sp_validRecipes:getChild("SearchBar/EditBox")

	self:AttachToModel()
	self:SetWindowState(false)

	self.m_currentSelectedRecipeWindow = nil

	self.m_HT_onRecipeChosenCallback = nil
end

-------------------------------------------------------------------------------
function HTCraftingMenuUIView:PostLoad(layout)
end

-------------------------------------------------------------------------------
function HTCraftingMenuUIView:AttachToModel()

end

-------------------------------------------------------------------------------
function HTCraftingMenuUIView:DetachFromModel()

end

-- Populate valid recipes scrollbox
-----------------------------------------------
function HTCraftingMenuUIView:HTRefreshRecipes(recipeDataArray)
	self.m_HT_recipeDataArray = recipeDataArray

	local ENTRY_HEIGHT = 40
	local ENTRY_MARGIN_SCALE = 0.0125
	local ICON_MARGIN_SCALE = 0.025

	-- TODO: add sorting based on recipe priority
	-- TODO: add optional catagorical sorting

	self.m_currentSelectedRecipeWindow = nil

	-- Remove all existing items
	if self.cont_validRecipes then
		self.sp_validRecipes:destroyChild(self.cont_validRecipes)
	end

	self.cont_validRecipes = EternusEngine.UI.Windows:createWindow("VerticalLayoutContainer")
	self.cont_validRecipes:setMouseInputPropagationEnabled(true)
	self.cont_validRecipes:setMousePassThroughEnabled(true)
	self.cont_validRecipes:setProperty("DistributeCapturedInputs","true")
	self.sp_validRecipes:addChild(self.cont_validRecipes)
	self.cont_validRecipes:setAlwaysOnTop(true)
	self.cont_validRecipes:moveToFront()

	-- Also clear out the currently selected recipe pane
	self:HTRefreshSelectedRecipe()

	-- CRAFTABLE RECIPES
	--------------------
	HTAddNewCategoryHeader({
		targetContainer = self.cont_validRecipes,
		text = "Craftable Recipes",
		entry_margin_scale = ENTRY_MARGIN_SCALE,
		entry_height = ENTRY_HEIGHT * 1.75,
		topPadding = 10
	})

	for recipeDataInd, recipeData in pairs(recipeDataArray) do
		local primaryResultQuantity = 1
		local primaryResultName = ""

		for resultDataInd, resultData in pairs(recipeData.results) do
			primaryResultName = resultData.name
			primaryResultQuantity = resultData.quantity
			break
		end

		-- Text
		local entryText = string.gsub(recipeData.recipeName," Recipe","")
		if primaryResultQuantity > 1 then
			entryText = entryText .. " x " .. tostring(primaryResultQuantity)
		end

		-- Callbacks
		local function lam_Clicked(eventArgs)
			local windowEventArgs = eventArgs:toWindowEventArgs()

			if self.m_currentSelectedRecipeWindow ~= windowEventArgs.window then
				-- If a different recipe is already selected we must reset its properties
				if self.m_currentSelectedRecipeWindow then
					HTSetButtonStateProperties(self.m_currentSelectedRecipeWindow,COLORS.RECIPE.IDLE)
				end

				self.m_currentSelectedRecipeWindow = windowEventArgs.window

				HTSetButtonStateProperties(windowEventArgs.window,COLORS.RECIPE.SELECTED)

				self:HTRefreshSelectedRecipe(self:GetSelectedRecipeData())
			end
		end

		-- Create the entry
		local newItem = HTAddNewImageButton({
			targetContainer = self.cont_validRecipes,
			text = entryText,
			entry_margin_scale = ENTRY_MARGIN_SCALE,
			entry_height = ENTRY_HEIGHT,
			icon = {
				name = HTGetIconNameFromObjectName(primaryResultName),
				margin_scale = ICON_MARGIN_SCALE
			},
			events = {
				Clicked = lam_Clicked
			}
		})

		-- User strings
		newItem:setUserString("recipeDataInd",tostring(recipeDataInd))
	end
	-- Recalculate the position of all recipe entries based on the settings of the vertical layout container
	CEGUI.toVerticalLayoutContainer(self.cont_validRecipes):layout()
end

-- Populate valid recipes scrollbox
-----------------------------------------------
function HTCraftingMenuUIView:HTRefreshSelectedRecipe(recipeData)
	local ENTRY_HEIGHT = 40
	local ENTRY_MARGIN_SCALE = 0.0125
	local ICON_MARGIN_SCALE = 0.025

	-- Remove all existing items
	if self.cont_selectedRecipe then
		self.sp_selectedRecipe:destroyChild(self.cont_selectedRecipe)
	end

	self.cont_selectedRecipe = EternusEngine.UI.Windows:createWindow("VerticalLayoutContainer")
	self.cont_selectedRecipe:setMouseInputPropagationEnabled(true)
	self.cont_selectedRecipe:setMousePassThroughEnabled(true)
	self.cont_selectedRecipe:setProperty("DistributeCapturedInputs","true")
	self.sp_selectedRecipe:addChild(self.cont_selectedRecipe)
	self.cont_selectedRecipe:setAlwaysOnTop(true)
	self.cont_selectedRecipe:moveToFront()

	if recipeData then
		-- CRAFT BUTTON
		--------------------
		-- Callbacks
		local function lam_CraftButton_Clicked(eventArgs)
			local recipeName = self:GetSelectedRecipeName()
			if string.len(recipeName) > 0 then
				self.m_HT_onRecipeChosenCallback(recipeName)
			end

			--PlaySound("Click1")
		end

		-- Create the entry
		HTAddNewImageButton({
			targetContainer = self.cont_selectedRecipe,
			text = "CRAFT THIS RECIPE",
			entry_margin_scale = ENTRY_MARGIN_SCALE,
			entry_height = ENTRY_HEIGHT * 2.5,
			--icon = {
			--	name = HTGetIconNameFromObjectName(primaryResultName),
			--	margin_scale = ICON_MARGIN_SCALE
			--},
			events = {
				Clicked = lam_CraftButton_Clicked
			}
		})

		-- Generate a dummy craft action so we can display the correct information
		local headerNeeded = true

		-- CONSUMED ITEMS
		--------------------
		headerNeeded = true
		for itemDataInd, itemData in pairs(recipeData.removals) do
			if headerNeeded then
				headerNeeded = false
				HTAddNewCategoryHeader({
					targetContainer = self.cont_selectedRecipe,
					text = "Ingredients to be Consumed",
					entry_margin_scale = ENTRY_MARGIN_SCALE,
					entry_height = ENTRY_HEIGHT * 1.75,
					topPadding = 10
				})
			end
			local newEntry = HTAddNewItemEntry({
				targetContainer = self.cont_selectedRecipe,
				itemName = itemData.name,
				entry_margin_scale = ENTRY_MARGIN_SCALE,
				entry_height = ENTRY_HEIGHT,
				icon = {
					margin_scale = ICON_MARGIN_SCALE
				}
			})

			local newEntryLabel = newEntry:getChildRecursive("Label")
			if newEntryLabel then
				local newText = string.format("%s - %i / %i",newEntryLabel:getText(),itemData.count,itemData.countAvailable)
				newEntryLabel:setText(newText)
			end
		end

		-- DURABILITY DAMAGED ITEMS
		--------------------
		headerNeeded = true
		for itemDataInd, itemData in pairs(recipeData.durabilityHits) do
			if headerNeeded then
				headerNeeded = false
				HTAddNewCategoryHeader({
					targetContainer = self.cont_selectedRecipe,
					text = "Ingredients That Will Lose Durability",
					entry_margin_scale = ENTRY_MARGIN_SCALE,
					entry_height = ENTRY_HEIGHT * 1.75,
					topPadding = 10
				})
			end
			local newEntry = HTAddNewItemEntry({
				targetContainer = self.cont_selectedRecipe,
				itemName = itemData.name,
				entry_margin_scale = ENTRY_MARGIN_SCALE,
				entry_height = ENTRY_HEIGHT,
				icon = {
					margin_scale = ICON_MARGIN_SCALE
				}
			})
			local labelWindow = newEntry:getChild("Label")

			if itemData.durability.current - itemData.durability.damage <= 0 then
				local newText = string.format("%s - Will be destroyed!",labelWindow:getText())
				labelWindow:setText(newText)
			else
				local percentCurrent = (itemData.durability.current / itemData.durability.max) * 100
				local percentUsed = (itemData.durability.damage / itemData.durability.max) * 100
				local newText = string.format("%s - %i%% / %i%%",labelWindow:getText(),percentUsed,percentCurrent)
				labelWindow:setText(newText)
			end
		end

		-- RESULTING ITEMS
		--------------------
		headerNeeded = true
		for itemDataInd, itemData in pairs(recipeData.results) do
			if headerNeeded then
				headerNeeded = false
				HTAddNewCategoryHeader({
					targetContainer = self.cont_selectedRecipe,
					text = "Results",
					entry_margin_scale = ENTRY_MARGIN_SCALE,
					entry_height = ENTRY_HEIGHT * 1.75,
					topPadding = 10
				})
			end
			HTAddNewItemEntry({
				targetContainer = self.cont_selectedRecipe,
				itemName = itemData.name,
				itemCount = itemData.quantity,
				entry_margin_scale = ENTRY_MARGIN_SCALE,
				entry_height = ENTRY_HEIGHT,
				icon = {
					margin_scale = ICON_MARGIN_SCALE
				}
			})
		end
	end

	-- Recalculate the position of all recipe entries based on the settings of the vertical layout container
	CEGUI.toVerticalLayoutContainer(self.cont_selectedRecipe):layout()
end

-------------------------------------------------------------------------------
function HTCraftingMenuUIView:AttachViewToModel(newModel)
	if self.m_containerViews[newModel:GetID()] then
		self.m_containerViews[newModel:GetID()]:AttachToModel(newModel)
	end
end

-------------------------------------------------------------------------------
function HTCraftingMenuUIView:DetachViewFromModel(oldModel)
	if self.m_containerViews[oldModel:GetID()] then
		self.m_containerViews[oldModel:GetID()]:DetachFromModel()
		self.m_containerViews[oldModel:GetID()]:Hide()
	end
end

-------------------------------------------------------------------------------
function HTCraftingMenuUIView:SetWindowState(open)
	if open then
		self:Show()
	else
		self:Hide()
	end
end

-------------------------------------------------------------------------------
function HTCraftingMenuUIView:GetSelectedRecipeData()
	if self.m_currentSelectedRecipeWindow then
		local recipeDataInd = tonumber(self.m_currentSelectedRecipeWindow:getUserString("recipeDataInd"))
		return self.m_HT_recipeDataArray[recipeDataInd]
	end
	return nil
end

-------------------------------------------------------------------------------
function HTCraftingMenuUIView:GetSelectedRecipeName()
	if self.m_currentSelectedRecipeWindow then
		return self:GetSelectedRecipeData().recipeName
	end
	return ""
end
