--require("debugger")("127.0.0.1","10000","luaidekey")

include("Scripts/Recipes/CraftingSystem.lua")
include("Scripts/Mixins/CraftingMixin.lua")
include("Scripts/Mixins/ServerCraftingMixin.lua")
include("Scripts/Mixins/ClientCraftingMixin.lua")
include("Scripts/Recipes/DefaultRecipe.lua")
include("Scripts/Characters/BasePlayer.lua")
include("Scripts/Characters/LocalPlayer.lua")

include("Scripts/HTCraftingMenuController.lua")

include("Scripts/HTUtil.lua")

-------------------------------------------------------------------------------
if HTCraftingMenu == nil then
	HTCraftingMenu = EternusEngine.ModScriptClass.Subclass("HTCraftingMenu")
end

-------------------------------------------------------------------------------
function HTCraftingMenu:Constructor()
end

 -------------------------------------------------------------------------------
 -- Called once from C++ at engine initialization time
function HTCraftingMenu:Initialize()
	--self:loadData()
	self:ApplyHooks()
end

-------------------------------------------------------------------------------
-- Called from C++ when the current game enters 
function HTCraftingMenu:Enter()	
end

-------------------------------------------------------------------------------
-- Called from C++ when the game leaves it current mode
function HTCraftingMenu:Leave()
end

-------------------------------------------------------------------------------
-- Called from C++ every update tick
function HTCraftingMenu:Process(dt)
end

-------------------------------------------------------------------------------
function HTCraftingMenu:loadData()
	-- Load new GUI resources
	if Eternus.IsClient then
		--local ret = EternusEngine.UI.Schemes:createFromFile("HTCraftingMenu.scheme")
	end
end

-------------------------------------------------------------------------------
function HTCraftingMenu:ApplyHooks()
	if Eternus.IsClient then
		LocalPlayer.StaticMixin(HTCraftingMenuController)
	end
end

-- //////////////////////////////////////////////////
-- ////////// CraftingSystem
-- //////////////////////////////////////////////////

-------------------------------------------------------------------------------
function CraftingSystem:FindAllValidRecipes(player)
	local objects = player:GetCraftableObjectsNearPosition()
	local playFailSound = false
	local validRecipes = ""

	for i = self.m_highestPriority, 0, -1 do
		if (self.m_recipes[i] ~= nil) then
			for objectToCraft,recipe in pairs(self.m_recipes[i]) do
				local satisfied, station, stationObj, failsound = recipe:IsRecipeSatisfied(objects,player)

				if not satisfied and failsound then
					playFailSound = true
				elseif satisfied then
					local delim = ""
					if string.len(validRecipes) > 0 then
						delim = "|"
					end
					validRecipes = validRecipes .. delim .. recipe:GetRecipeName()
				end
			end
		end
	end

	if (string.len(validRecipes) == 0) then
		if (playFailSound) then
			return nil, nil, "failed"
		else
			return nil, nil, "invalid"
		end
	else
		return validRecipes, objects
	end
end

-------------------------------------------------------------------------------
function CraftingSystem:FindRecipeObject(recipeName)
	for i = self.m_highestPriority, 0, -1 do
		if self.m_recipes[i] then
			local recipe = self.m_recipes[i][ recipeName ]
			if recipe then
				return recipe
			end
		end
	end
end

-- //////////////////////////////////////////////////
-- ////////// DefaultRecipe
-- //////////////////////////////////////////////////

-- Must override method for custom logic, as crafting stations that are in the player's inventory would
-- be considered valid by default
-------------------------------------------------------------------------------
if DefaultRecipe.IsRecipeSatisfied and not DefaultRecipe.HT_DEF_IsRecipeSatisfied then
	DefaultRecipe.HT_DEF_IsRecipeSatisfied = DefaultRecipe.IsRecipeSatisfied;
	function DefaultRecipe:IsRecipeSatisfied(areaObjects,player)
		self.m_HT_currentPlayer = player
		local satisfied, station, stationObj, failsound = self:HT_DEF_IsRecipeSatisfied(areaObjects,player)
		self.m_HT_currentPlayer = nil

		return satisfied, station, stationObj, failsound
	end
end

-- Must override method for custom logic, as crafting statinos that are in the player's inventory would
-- be considered valid by default
-------------------------------------------------------------------------------
if DefaultRecipe.HasActiveCrafter and not DefaultRecipe.HT_DEF_HasActiveCrafter then
	DefaultRecipe.HT_DEF_HasActiveCrafter = DefaultRecipe.HasActiveCrafter;
	function DefaultRecipe:HasActiveCrafter(objectName,areaObjects)
		-- If custom logic is enabled, ensure that the targeted crafting station is
		if self.m_HT_currentPlayer and self.m_HT_currentPlayer:HTShouldUseCraftingMenuLogic() then
			for key,value in pairs(areaObjects) do
				if value:NKGetName() == objectName then
					local instance = value:NKGetInstance()
					if instance.m_activeCrafter == true and not instance:InInventory() then
						return value
					end
				end
			end

			return nil
		else
			return self:HT_DEF_HasActiveCrafter(objectName,areaObjects)
		end
	end
end

-- Override method to ensure that the result items' placement is not corrupted by
-- the erronious reported position of items that are in the player's inventory
-------------------------------------------------------------------------------
if DefaultRecipe.DoRemovals and not DefaultRecipe.HT_DEF_DoRemovals then
	DefaultRecipe.HT_DEF_DoRemovals = DefaultRecipe.DoRemovals;
	function DefaultRecipe:DoRemovals(craftAction,player,placePosition)
		if player:HTShouldUseCraftingMenuLogic() then
			local lastRemovalPosition = placePosition
			for key, value in pairs( craftAction.removals ) do
				key:NKGetInstance():ModifyStackSize(-value.stackCount)

				-- CHANGE: added "and key:NKIsInWorld()"
				if not craftAction.station and key:NKIsInWorld() then
					lastRemovalPosition = key:NKGetPosition() + vec3.new(0.0, 1.0, 0.0)
				end
			end

			-- Do durability damage to unconsumed objects that are part of the recipie
			for key,value in pairs( craftAction.durabilityHits ) do
				key:NKGetInstance():ModifyHitPoints(-self.m_unconsumedDamage)
			end
			return lastRemovalPosition
		else
			return self:HT_DEF_DoRemovals(craftAction,player,placePosition)
		end
	end
end

-- //////////////////////////////////////////////////
-- ////////// ServerCraftingMixin
-- //////////////////////////////////////////////////

BasePlayer.RegisterScriptEvent("SE_HT_RequestCraftingMenu", 
	{
		at = "vec3",
	}
)

BasePlayer.RegisterScriptEvent("SE_HT_RequestCraftingSpecific", 
	{
		at = "vec3",
		recipe = "string",
	}
)

BasePlayer.RegisterScriptEvent("SE_HT_CraftingMenuClosed", 
	{
	}
)

-------------------------------------------------------------------------------
function ServerCraftingMixin:HTShouldUseCraftingMenuLogic()
	return self.m_HT_useCraftingMenuLogic or (self.m_craftAction and self.m_craftAction.m_HT_useCraftingMenuLogic)
end

-- Override method to add the contents of a player's inventory to the list of
-- craftable items reported to be in their vicinity
-------------------------------------------------------------------------------
if ServerCraftingMixin.GetCraftableObjectsNearPosition and not ServerCraftingMixin.HT_DEF_GetCraftableObjectsNearPosition then
	ServerCraftingMixin.HT_DEF_GetCraftableObjectsNearPosition = ServerCraftingMixin.GetCraftableObjectsNearPosition;
	function ServerCraftingMixin:GetCraftableObjectsNearPosition()
		local areaObjects = self:HT_DEF_GetCraftableObjectsNearPosition()
		if self:HTShouldUseCraftingMenuLogic() then
			-- Loop through all inventory containers within the found controller and add thier
			-- contents to areaObjects
			for containerIndex,container in pairs(self.m_inventoryContainers) do
				for itemIndex,item in pairs(container.m_items) do
					
					local root = nil
					-- cant belive lua doesnt have a ternary operator...
					if item and item.m_item then
						local native = item.m_item
						--root = native:NKGetRootParent() or native
						-- calling NKGetRootParent() on some (all?) in-hand items on a dedicated server causes an infinite loop
						root = native
					end
					if root and root:NKGetPlaceable() and not NKUtils.TableFindKey(areaObjects,root) then
						table.insert(areaObjects,root)
						areaObjects[#areaObjects].m_HT_inventoryContainer = container
					end
				end
			end
		end

		return areaObjects
	end
end

-- Override method to add the contents of a player's inventory to the list of
-- craftable items reported to be in their vicinity
-------------------------------------------------------------------------------
if ServerCraftingMixin.ServerEvent_RequestCrafting and not ServerCraftingMixin.HT_DEF_ServerEvent_RequestCrafting then
	ServerCraftingMixin.HT_DEF_ServerEvent_RequestCrafting = ServerCraftingMixin.ServerEvent_RequestCrafting;
	function ServerCraftingMixin:ServerEvent_RequestCrafting(args)
		-- Ensure attempts to craft normally are not affected by this mod
		self.m_HT_useCraftingMenuLogic = false

		self:HT_DEF_ServerEvent_RequestCrafting(args)
	end
end

-- Find all recipes that can currently be crafted by the player and send a return
-- message to display them in the crafting menu
-------------------------------------------------------------------------------
function ServerCraftingMixin:SE_HT_RequestCraftingMenu(args)
	if not self.m_serverIsCrafting then
		-- Ensure we use the proper crafting logic
		self.m_HT_useCraftingMenuLogic = true

		self.m_currentCraftingLocation  = args.at
		
		local recipes, areaObjects, err = Eternus.CraftingSystem:FindAllValidRecipes(self)
		recipes = recipes or ""
		self.m_HT_currentCraftingAreaObjects = areaObjects

		--build data
		local menuData = {}
		local recipeArray = string.split(recipes,"|")
		for recipeInd, recipeName in pairs(recipeArray) do
			local recipeObject = Eternus.CraftingSystem:FindRecipeObject(recipeName)

			if recipeObject then
				local dummyCraftAction = recipeObject:GenerateCraftAction(areaObjects,self,args.at)
				
				menuData[recipeInd] = {}
				menuData[recipeInd].recipeName = recipeName
				menuData[recipeInd].removals = {}
				for itemRef, itemCount in pairs(dummyCraftAction.removals) do
					local totalAvailable = 0
					for key,value in pairs(areaObjects) do
						if ((value:NKGetName() == itemRef:NKGetName()) and value:NKGetParent() == nil) then
							totalAvailable = totalAvailable + value:NKGetInstance():GetStackSize()
						end
					end

					local removalData = {
						name = itemRef:NKGetName(),
						count = itemCount.stackCount,
						countAvailable = totalAvailable
					}
					table.insert(menuData[recipeInd].removals,removalData)
				end
				
				menuData[recipeInd].durabilityHits = {}
				for itemRef, itemCount in pairs(dummyCraftAction.durabilityHits) do
					local instance = itemRef:NKGetInstance()
					local durabilityData = {
						name = itemRef:NKGetName(),
						durability = {
							max = instance:GetMaxHitPoints(),
							current = instance:GetHitPoints(),
							damage = recipeObject.m_unconsumedDamage
						}
					}
					table.insert(menuData[recipeInd].durabilityHits,durabilityData)
				end

				menuData[recipeInd].results = {}
				for resultItemName, resultQuantity in pairs(recipeObject.m_results) do
					local resultData = {
						name = resultItemName,
						quantity = resultQuantity
					}
					table.insert(menuData[recipeInd].results,resultData)
				end
			end
		end
		local dataString = table.val_to_str(menuData)

		self:RaiseClientEvent("CE_HT_DisplayCraftingMenu",{ at = args.at, validRecipes = recipes, data = dataString })

		if err == "failed" then
			--self:RaiseClientEvent("ClientEvent_FailedCrafting", {})
		end
	end
end

-- Recieved after the client closes its crafting menu
-------------------------------------------------------------------------------
function ServerCraftingMixin:SE_HT_CraftingMenuClosed()
	-- Ensure attempts to craft normally are not affected by this mod
	self.m_HT_useCraftingMenuLogic = false
end

-------------------------------------------------------------------------------
function ServerCraftingMixin:SE_HT_RequestCraftingSpecific(args)
	if not self.m_serverIsCrafting then
		self.m_currentCraftingLocation  = args.at

		local recipe = Eternus.CraftingSystem:FindRecipeObject(args.recipe)
		if recipe ~= nil then
			local craftAction = recipe:GenerateCraftAction(self.m_HT_currentCraftingAreaObjects,self)
			if craftAction then
				-- Ensure we use the proper crafting logic
				craftAction.m_HT_useCraftingMenuLogic = true

				self:StartCrafting(craftAction)
			else
				err = "failed"
			end
		end

		if err == "failed" then
			self:RaiseClientEvent("ClientEvent_FailedCrafting", {})
		end
	else
		--Finished.
		self:CraftingInterrupted()
		self:RaiseClientEvent("ClientEvent_StopCrafting", { failed = true })
	end
end

-- //////////////////////////////////////////////////
-- ////////// ClientCraftingMixin
-- //////////////////////////////////////////////////

BasePlayer.RegisterScriptEvent("CE_HT_DisplayCraftingMenu", 
	{
		at = "vec3",
		validRecipes = "string",
		data = "string",
	}
)

-------------------------------------------------------------------------------
function ClientCraftingMixin:CE_HT_DisplayCraftingMenu(args)
	self.m_HT_craftPosition = args.at
	assert(loadstring("recipeDataArray = " .. args.data))()
	self:OpenCraftingMenu(recipeDataArray)
end

EntityFramework:RegisterModScript(HTCraftingMenu)