include("Scripts/UI/HTCraftingMenuUIView.lua")

if HTCraftingMenuController == nil then
	HTCraftingMenuController = EternusEngine.Mixin.Subclass("HTCraftingMenuController")
end

-------------------------------------------------------------------------------
function HTCraftingMenuController:Constructor()
	-- Hook player's SetupInputContext function to insert our crafting menu toggle button
	-- Doing this via a mixin function of the same name would not work, as the functions would
	-- be chained such that the new one is called before the old, which is undesireable.
	if self.SetupInputContext and not self.HT_DEF_SetupInputContext then
		self.HT_DEF_SetupInputContext = self.SetupInputContext;
		function self.SetupInputContext(self)
			self:HT_DEF_SetupInputContext()
			self.m_defaultInputContext:NKRegisterNamedCommand("Toggle HT Crafting Menu",self,"HTToggleCraftingMenu",KEY_ONCE)
		end
	end

	self.m_HT_showCraftingMenu = false;
	self.m_HT_craftingMenuToggleable = true;
end

-------------------------------------------------------------------------------
function HTCraftingMenuController:SetupUI(...)
	self.m_HT_craftingMenuUI = HTCraftingMenuUIView.new("CraftingWindow.layout",self)
	self.m_HT_craftingMenuUI:Hide()
end

-------------------------------------------------------------------------------
function HTCraftingMenuController:HTCreateCraftingWindowContext()
	self.m_HT_craftingMenuContext = InputMappingContext.new("Inventory")
	self.m_HT_craftingMenuContext:NKSetInputPropagation(false)
	self.m_HT_craftingMenuContext:NKRegisterNamedCommand("Toggle HT Crafting Menu",self,"HTToggleCraftingMenu",KEY_ONCE)
	self.m_HT_craftingMenuContext:NKRegisterNamedCommand("Return to Menu",self,"HTToggleCraftingMenuOverride",KEY_ONCE)
end

-------------------------------------------------------------------------------
function HTCraftingMenuController:OpenCraftingMenu(recipeDataArray)
	if self:IsDead() then
		return
	end

	if not self.m_HT_craftingMenuToggleable then
		return
	end

	if not self.m_HT_showCraftingMenu then
		self.m_HT_showCraftingMenu = true;

		-- Push the crafting menu input context
		self:HTCreateCraftingWindowContext()
		Eternus.InputSystem:NKPushInputContext(self.m_HT_craftingMenuContext)

		-- Prepare extraneous UI elements
		self.m_gameModeUI.m_crosshair:hide()
		self.m_playerUI:Hide();
		Eternus.InputSystem:NKShowMouse()
		Eternus.InputSystem:NKCenterMouse()

		-- Show the crafting menu UI
		self.m_HT_craftingMenuUI:SetWindowState(true)
		self.m_HT_craftingMenuUI:HTRefreshRecipes(recipeDataArray)
		self.m_HT_craftingMenuUI.m_HT_onRecipeChosenCallback = function(selectedRecipeName)
			self:RaiseServerEvent("SE_HT_RequestCraftingSpecific",{ at = self.m_HT_craftPosition, recipe = selectedRecipeName })
			self:CloseCraftingMenu()
		end
	end
end

-------------------------------------------------------------------------------
function HTCraftingMenuController:CloseCraftingMenu()
	if self.m_HT_showCraftingMenu then
		self.m_HT_showCraftingMenu = false;

		-- Pop the crafting menu input context
		Eternus.InputSystem:NKRemoveInputContext(self.m_HT_craftingMenuContext)

		-- Undo changes to extraneous UI elements
		self.m_gameModeUI.m_crosshair:show()
		self.m_playerUI:Show();
		Eternus.InputSystem:NKHideMouse()

		-- Hide the crafting menu UI
		self.m_HT_craftingMenuUI:SetWindowState(false)

		self:RaiseServerEvent("SE_HT_CraftingMenuClosed",{})
	end
end

-- EVENTS
-------------------------------------------------------------------------------
function HTCraftingMenuController:HTToggleCraftingMenuOverride(down)
	if down then
		return
	end

	self:CloseCraftingMenu()
end
function HTCraftingMenuController:HTToggleCraftingMenu(down)
	if down then
		return
	end
	
	if not self.m_HT_showCraftingMenu then
		local eyePosition = self:NKGetPosition() + vec3(0.0,self.m_cameraHeight,0.0)
		local lookDirection = Eternus.GameState.m_activeCamera:ForwardVector()
		local result = NKPhysics.RayCastCollect(eyePosition,lookDirection,self:GetMaxReachDistance(),{self})

		local tracePos
		if result then
			tracePos = result.point
		else
			tracePos = eyePosition
			tracePos = tracePos + (lookDirection:mul_scalar(2.5))
		end

		self:RaiseServerEvent("SE_HT_RequestCraftingMenu",{ at = tracePos})
	else
		self:CloseCraftingMenu()
	end
end