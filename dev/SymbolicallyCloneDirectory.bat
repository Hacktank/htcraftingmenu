@echo off

setlocal EnableDelayedExpansion EnableExtensions
SET rootDirToClone=%~1
SET dirToClone=%~2
SET dirCOMBO=%~3

pushd %dirCOMBO% >nul || exit /b

set dirToCreate=$!dirToClone:%rootDirToClone%=!
set dirToCreate=!dirToCreate:$\=!
set dirToCreate="!dirToCreate:$=!"

if not %dirToCreate%=="" (
	if not exist %dirToCreate% (
		echo making directory: %dirToCreate%
		mkdir %dirToCreate%
	)
)

pushd %dirToCreate% >nul || exit /b
for %%I in ("%dirToClone%\*.*") do mklink "%%~nxI" "%%I"
popd

popd

endlocal
goto :eof