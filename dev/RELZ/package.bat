@ECHO off

SET VERSION=0.0.2
SET MODNAME=HTCraftingMenu

SET ARCHIVENAME=archives\%MODNAME%_%VERSION%.7z

SET PDATA=%CD%\..\..
SET PPACKAGE=%CD%\package\%MODNAME%

DEL %ARCHIVENAME% /Q

RMDIR "%PPACKAGE%" /S /Q
MKDIR "%PPACKAGE%"

:: Readme
COPY "README.txt" "%PPACKAGE%\" /Y

:: Manifest
COPY "%PDATA%\manifest.txt" "%PPACKAGE%\" /Y

:: Data
XCOPY "%PDATA%\Data" "%PPACKAGE%\Data" /S /Y /I /EXCLUDE:exclude.txt

:: UI Data
XCOPY "%PDATA%\UI" "%PPACKAGE%\UI" /S /Y /I /EXCLUDE:exclude.txt

:: Scripts
XCOPY "%PDATA%\Scripts" "%PPACKAGE%\Scripts" /S /Y /I /EXCLUDE:exclude.txt

"C:\Program Files\7-Zip\7z.exe" a -t7z -r "%ARCHIVENAME%" "%PPACKAGE%\..\*.*"

RMDIR "%PPACKAGE%" /S /Q
