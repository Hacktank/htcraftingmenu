HTCraftingMenu V0.0.1

Creator contact info:
Email: Jacob.N.Tyndall@GMail.com

Alpha release.

To install extract this archive into your ...\TUG\Mods\ directory and add it to the list of mods to be loaded in your ...\TUG\config\mods.txt file like so:

Mods
{
	"Game/Dev"
	"Game/Creative"
	"Game/Survival"
	"Game/Core"
	"Mods/HTCraftingMenu"
}

USAGE----------------------------------------------

To open the crafting menu press the V key, this can be changed in the normal game keybind settings accessed via the escape options menu.

Click on a recipe that you would like to craft from the left window and information about it will appear on the right window. If you want to actually craft this recipe click on the craft this recipe button at the top of the right window.