@echo off

rem Arguments: Directory Func
rem Directory = Starting directory
rem Func = Function to call, occurrences of TRAVDIR will be replaced with the currently visited directory

setlocal
set RootDir=%~1
pushd "%RootDir%" >nul || exit /b

rem directory name == TRAVDIR
set VisitCommand=%~2

call :traverseDirectoryDepthFirst

popd
endlocal
goto :eof

:traverseDirectoryDepthFirst
setlocal EnableDelayedExpansion EnableExtensions
set currentLevel=0
set maxLevel=%2
if not defined maxLevel set maxLevel=50

call :procFolder

rem also hit current directory
set IMPLVisitCommand=!VisitCommand:TRAVDIR=%RootDir%!
rem Echange any escaped quotes (double) with a single quote to allow calling
rem PROBLEM AREA !!!! if the command was supposed to have escaped quotes ine it it will probably break
set IMPLVisitCommand=!IMPLVisitCommand:""="!
call %%IMPLVisitCommand%%

goto :eof

:procFolder
pushd %1 >nul || exit /b
if %currentLevel% lss %maxLevel% (
  for /d %%F in (*) do (
    set /a currentLevel+=1
    call :procFolder "%%F"
    set /a currentLevel-=1
	
	rem Swap TRAVDIR with the current directory
	set IMPLVisitCommand=!VisitCommand:TRAVDIR=%%~fF!
	rem Echange any escaped quotes (double) with a single quote to allow calling
	rem PROBLEM AREA !!!! if the command was supposed to have escaped quotes ine it it will probably break
	set IMPLVisitCommand=!IMPLVisitCommand:""="!
	call %%IMPLVisitCommand%%
  )
)
popd
goto :eof