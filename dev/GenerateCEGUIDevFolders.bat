@echo off

setlocal
SET workingDir=%CD%

SET dirA=%TUG%\Game\Core\UI\GuiDatafiles
SET dirB=..\UI\GuiDatafiles

rem dirA and dirB MUST BE ABSOLUTE, DIRECT PATHS
pushd %dirA%
set dirA=%cd%
popd

pushd %dirB%
set dirB=%cd%
popd

SET dirCOMBO=%cd%\CEGUIDEV

if not exist "%dirCOMBO%" mkdir "%dirCOMBO%"

pushd %dirCOMBO% >nul || exit /b
del /f /q *.*
FOR /D %%p IN ("*.*") DO rmdir "%%p" /s /q
popd

call "%workingDir%\DirectoryDFS.bat" "%dirA%" "call ""%workingDir%\SymbolicallyCloneDirectory.bat"" ""%dirA%"" ""TRAVDIR"" ""%dirCOMBO%"""
call "%workingDir%\DirectoryDFS.bat" "%dirB%" "call ""%workingDir%\SymbolicallyCloneDirectory.bat"" ""%dirB%"" ""TRAVDIR"" ""%dirCOMBO%"""

endlocal
goto :eof